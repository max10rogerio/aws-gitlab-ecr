import express from "express";
import cors from "cors";

import { env } from "./config/env.mjs";

// create express app
const app = express();

// register middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// add cors
app.use(cors());

// register routes
app.get("/", (req, res) => {
  return res.send("Hello Caique!");
});

// start express server on port 5000
app.listen(env.port, () => {
  console.log("server started on port ", env.port);
});
